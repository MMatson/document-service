package com.mark.docs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mark.docs.model.DocumentRequest;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.concurrent.TimeUnit;


public class DocumentRequestResourceTest {
    private HttpServer server;
    private WebTarget target;
    private String testDocumentID;

    @Before
    public void setUp() throws Exception {
        server = Main.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(Main.BASE_URI);

        DocumentRequest testDocument = new DocumentRequest("test");
        testDocumentID = testDocument.getID();
        DocumentRequestStore.getInstance().putDocumentRequest(testDocument);
    }

    @After
    public void tearDown() throws Exception {
        server.shutdownNow();
    }

    @Test
    public void testPostNewDocumentRequest() {
        Response response = target.path("/documentRequests/request/").request()
                .post(Entity.entity("{ \"body\": \"test body\"}", "application/json"));
        //Upon success check that response body contains valid UUID of created document request
        Assert.assertTrue(response.readEntity(String.class).matches(Main.UUID_REGEX));
    }

    @Test
    public void testPostDocumentRequestStarted() {
        /*
        Because I never exposed the callback ID to the client, I can't test the endpoint directly (no time to refactor).
        However, posting a new doc request is guaranteed to cause the mock TPS to call this endpoint.
        So, if the status is "STARTED" and not "CREATED", this test must be successful.
         */
        Response response = target.path("/documentRequests/request/").request()
                .post(Entity.entity("{ \"body\": \"test body\"}", "application/json"));
        String id = response.readEntity(String.class);
        String responseBody = target.path("/documentRequests/status/" + id).request().get(String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readTree(responseBody);
            Assert.assertEquals("STARTED", node.get("status").asText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateDocumentRequestStatus() throws InterruptedException {
        /*
        Similar to above, the mock TPS is guaranteed to call this API 30 seconds after a new doc request is posted.
        Hence, we just check to see if the status has changed to one of the updated values after the callback occurs.
         */
        String statusRegEx = "PROCESSED|COMPLETED|ERROR";

        Response response = target.path("/documentRequests/request/").request()
                .post(Entity.entity("{ \"body\": \"test body\"}", "application/json"));
        String id = response.readEntity(String.class);

        //Wait at least 10 seconds for callback to occur
        TimeUnit.SECONDS.sleep(12);

        String responseBody = target.path("/documentRequests/status/" + id).request().get(String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readTree(responseBody);
            Assert.assertTrue(node.get("status").asText().matches(statusRegEx));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetDocumentRequestStatus() {
        String responseBody = target.path("/documentRequests/status/" + testDocumentID).request().get(String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode node = objectMapper.readTree(responseBody);
            Assert.assertEquals("CREATED", node.get("status").asText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}