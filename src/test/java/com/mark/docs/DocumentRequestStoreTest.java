package com.mark.docs;

import com.mark.docs.model.DocumentRequest;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.UUID;

public class DocumentRequestStoreTest {

    @Test
    public void testGetDocumentRequest() {
        DocumentRequestStore store = DocumentRequestStore.getInstance();
        DocumentRequest request = new DocumentRequest("test");
        String requestID = request.getID();
        store.putDocumentRequest(request);
        Assert.assertEquals(request, store.getDocumentRequest(requestID));
    }

    @Test
    public void testGetDocumentRequestFailed() {
        DocumentRequestStore store = DocumentRequestStore.getInstance();
        String requestID = UUID.randomUUID().toString();
        Assert.assertNull(store.getDocumentRequest(requestID));
    }

    @Test
    //Testing successful update of existing request in store
    public void testPutDocumentRequest() {
        DocumentRequestStore store = DocumentRequestStore.getInstance();
        DocumentRequest request = new DocumentRequest("test");
        String requestID = request.getID();
        store.putDocumentRequest(request);
        request = store.getDocumentRequest(requestID);
        request.setStatus("Updated");
        store.putDocumentRequest(request);
        Assert.assertEquals("Updated", store.getDocumentRequest(requestID).getStatus());
    }

    @Test
    public void testGetDocumentRequestByCallbackID() {
        DocumentRequestStore store = DocumentRequestStore.getInstance();
        DocumentRequest request = new DocumentRequest("test");
        String callbackID = request.getCallbackID();
        store.putDocumentRequest(request);
        Assert.assertEquals(request, store.getDocumentRequestByCallbackID(callbackID));
    }

}