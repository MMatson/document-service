package com.mark.docs;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mark.docs.model.DocumentRequest;
import com.mark.docs.model.DocumentStatusPayload;
import com.mark.docs.model.ThirdPartyRequestPayload;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/*
    Root resource for all document request related endpoints
 */
@Path("/documentRequests")
public class DocumentRequestResource {

    private static final String THIRD_PARTY_URI = "http://localhost:8080/thirdPartyService/request";
    private static final DocumentRequestStore STORE = DocumentRequestStore.getInstance();

    @POST @Path("/request")
    @Consumes("application/json")
    public String postNewDocumentRequest(String body) {
        //Logging

        ObjectMapper objectMapper = new ObjectMapper();
        String documentBody;
        try {
            JsonNode node = objectMapper.readTree(body);
            documentBody = node.get("body").asText();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "Cannot parse document body.";
        }
        DocumentRequest request = new DocumentRequest(documentBody);
        STORE.putDocumentRequest(request);

        //Generate Third Party Service Request
        try {
            URL url = new URL(THIRD_PARTY_URI);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            ThirdPartyRequestPayload payload = new ThirdPartyRequestPayload();
            payload.setBody(request.getBody());
            payload.setCallback(request.getCallbackID());
            String payloadString = objectMapper.writeValueAsString(payload);

            OutputStream outputStream = connection.getOutputStream();
            byte[] input = payloadString.getBytes(StandardCharsets.UTF_8);
            outputStream.write(input, 0, input.length);

            if (connection.getResponseCode() == 204) {
                //Success!
                return request.getID();
            } else {
                return "Service did not accept document request.";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed to connect to third party service.";
        }
    }


    @POST @Path("/callback/{id: " + Main.UUID_REGEX + "}")
    @Consumes("text/plain")
    public Response postDocumentRequestStarted(@PathParam("id") String id, String body) {
        //Logging

        DocumentRequest request = STORE.getDocumentRequestByCallbackID(id);

        if (request != null && body.equals("STARTED")) {
            request.setStatus("STARTED");
            STORE.putDocumentRequest(request);
            return Response.status(204).build();
        } else {
            return Response.status(404).build();
        }
    }


    @PUT @Path("/callback/{id: " + Main.UUID_REGEX + "}")
    @Consumes("application/json")
    public Response updateDocumentRequestStatus(@PathParam("id") String id, String body) {
        //Logging

        ObjectMapper objectMapper = new ObjectMapper();
        String status, detail;
        try {
            JsonNode node = objectMapper.readTree(body);
            status = node.get("status").asText();
            detail = node.get("detail").asText();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Response.status(400).build();
        }

        DocumentRequest request = STORE.getDocumentRequestByCallbackID(id);
        if (request == null) { return Response.status(404).build(); }

        request.setStatus(status);
        request.setDetail(detail);
        STORE.putDocumentRequest(request);

        return Response.status(204).build();
    }


    @GET @Path("/status/{id: " + Main.UUID_REGEX + "}")
    @Produces("application/json")
    public String getDocumentRequestStatus(@PathParam("id") String id) {
        //Logging

        DocumentRequest request = STORE.getDocumentRequest(id);
        if (request == null) { return "Request " + id + " not found."; }

        DocumentStatusPayload payload = new DocumentStatusPayload();
        payload.setStatus(request.getStatus());
        payload.setDetail(request.getDetail());
        payload.setBody(request.getBody());
        payload.setCreatedAt(request.getCreatedAt().toString());
        payload.setLastUpdatedAt(request.getLastUpdatedAt().toString());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String payloadString = objectMapper.writeValueAsString(payload);
            return payloadString;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.out.println("Failed to process string as JSON: " + payload);
            return "Error processing request.";
        }
    }

}
