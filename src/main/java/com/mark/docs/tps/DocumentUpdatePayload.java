package com.mark.docs.tps;

public class DocumentUpdatePayload {
    private String status;
    private String detail;

    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    public String getDetail() { return detail; }
    public void setDetail(String detail) { this.detail = detail; }
}
