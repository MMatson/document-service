package com.mark.docs.tps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/*
    Mock third party server to facilitate document processing callbacks
 */
@Path("/thirdPartyService")
public class ThirdPartyServiceResource {

    private static final String DOC_SERVER_URI = "http://localhost:8080/documentRequests/callback";

    private static final String[] STATUSES = new String[] { "STARTED", "PROCESSED", "COMPLETED", "ERROR" };
    private static final String DETAILS = "Lorem ipsum...";

    @POST @Path("/request")
    @Consumes("application/json")
    public Response postNewDocument(String body) {
        ObjectMapper objectMapper = new ObjectMapper();
        String callbackID;
        try {
            JsonNode node = objectMapper.readTree(body);
            callbackID = node.get("callback").asText();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return Response.status(400).build();
        }

        //Post started status
        try {
            URL url = new URL((DOC_SERVER_URI + "/" + callbackID));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setDoOutput(true);

            OutputStream outputStream = connection.getOutputStream();
            byte[] input = STATUSES[0].getBytes(StandardCharsets.UTF_8);
            outputStream.write(input, 0, input.length);

            if (connection.getResponseCode() != 204) {
                System.out.println(connection.getResponseCode());
                return Response.status(400).build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(400).build();
        }

        //Mock out async call from third party service back to application
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(1);

        Runnable provideUpdatedDocumentStatus = () -> {
            try {
                URL url = new URL((DOC_SERVER_URI + "/" + callbackID));
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);

                int random = ThreadLocalRandom.current().nextInt(1, 4);
                DocumentUpdatePayload payload = new DocumentUpdatePayload();
                payload.setStatus(STATUSES[random]);
                payload.setDetail(DETAILS);

                String payloadString = objectMapper.writeValueAsString(payload);

                OutputStream outputStream = connection.getOutputStream();
                byte[] input = payloadString.getBytes(StandardCharsets.UTF_8);
                outputStream.write(input, 0, input.length);

                if (connection.getResponseCode() != 204) {
                    System.out.println(connection.getResponseCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        };

        //Process doc after a 10-second delay
        threadPool.schedule(provideUpdatedDocumentStatus, 10, TimeUnit.SECONDS);
        threadPool.shutdown();

        return Response.status(204).build();
    }

}
