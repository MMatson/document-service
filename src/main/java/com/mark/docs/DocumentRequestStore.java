package com.mark.docs;

import com.mark.docs.model.DocumentRequest;

import java.util.HashMap;
import java.util.Map;

//This class would read/write to database
public class DocumentRequestStore {
    private final Map<String, DocumentRequest> requestMap = new HashMap<>();
    private final Map<String, String> callbackMap = new HashMap<>();

    private static final DocumentRequestStore instance = new DocumentRequestStore();

    //Singleton Class
    public static DocumentRequestStore getInstance() { return instance; }
    private DocumentRequestStore() {}

    public DocumentRequest getDocumentRequest(String id) { return requestMap.get(id); }

    public DocumentRequest getDocumentRequestByCallbackID(String id) {
        return requestMap.get(callbackMap.get(id));
    }

    public void putDocumentRequest(DocumentRequest request) {
        requestMap.put(request.getID(), request);
        callbackMap.put(request.getCallbackID(), request.getID());
    }
}
