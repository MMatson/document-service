package com.mark.docs.model;

import java.time.Instant;
import java.util.UUID;

public class DocumentRequest {
    private final String id;
    private final String body;
    private String status;
    private String detail;
    private final String callbackID;

    private final Instant createdAt;
    private Instant lastUpdatedAt;

    public DocumentRequest(String body) {
        this.id = UUID.randomUUID().toString();
        this.callbackID = UUID.randomUUID().toString();
        this.body = body;
        this.status = "CREATED";
        this.detail = "NULL";
        this.createdAt = Instant.now();
        this.lastUpdatedAt = Instant.now();
    }

    public String getID() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        this.lastUpdatedAt = Instant.now();
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
        this.lastUpdatedAt = Instant.now();
    }

    public String getCallbackID() {
        return callbackID;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getLastUpdatedAt() {
        return lastUpdatedAt;
    }
}
