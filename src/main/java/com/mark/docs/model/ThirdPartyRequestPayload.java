package com.mark.docs.model;

public class ThirdPartyRequestPayload {
    private String body;
    private String callback;

    public String getBody() { return body; }
    public void setBody(String body) { this.body = body; }

    public String getCallback() { return callback; }
    public void setCallback(String callback) { this.callback = callback; }
}
