# Backend Exercise


## Requires

Java, Maven


## Instructions

In the project directory, you can run:

### `mvn package`

to build the .jar file. Then run:

### `java -jar ./target/document-service-1.0-jar-with-dependencies.jar`

Starts the HTTP server at http://localhost:8080/documentRequests/


## Important

A mock third party service will respond back to a document request automatically after 10 seconds 
and will change the status to one of "PROCESSED", "COMPLETED", or "ERROR".


## Other Notes

 - I did not implement logging on the endpoints, only noted some instances. 
I would have logs on all the endpoints in production.
 - I would have also liked to do some additional functional decomposition 
and delineation of architectural layers. 